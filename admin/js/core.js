(function($) {
  //"use strict";

  $(function() {
     

    $('#save-profile').click(function(){
      	
      	var data={}; 
  
        data.name_apps    =    $('#wpsalmonbrain-option-name-apps').val();
        data.app_id       =    $('#wpsalmonbrain-option-fb-app-id').val();
        data.app_secret   =    $('#wpsalmonbrain-option-fb-app-secret').val(); 

        if(  data.name_apps == '' ) {
          alert( 'NOt empty'); 
        }
        else {

       	$('#loading').show().html('<span class="spinner is-active"></span>'); 
        $('#submit').prop('disabled', true);

        data.action = "ajax_save_profile";
       	jQuery.post(ajaxurl,data,function(response){
          
          var modal = UIkit.modal(".uk-modal");
          modal.hide();

          $('.result tr:first').after(response);

        }).done(function() {
            $('#submit').prop('disabled', false);
            $('input').val('');
            $('#loading').slideUp( 300 ).delay( 200 ).fadeIn( 200 ).html('Done'); 
        });

        } 

     });

      $('#edit-profile').click(function(){
        
        var data={}; 

      /******* create blank page ******/
        $('#loading').show().html('<span class="spinner is-active"></span>'); 
        $('#submit').prop('disabled', true);

        data.redirect           =   $('#redirect').val();

        data.id           =   $('#wpsalmonbrain-option-id').val();
        data.name_apps    =    $('#wpsalmonbrain-option-name-apps').val();
        data.app_id       =    $('#wpsalmonbrain-option-fb-app-id').val();
        data.app_secret   =    $('#wpsalmonbrain-option-fb-app-secret').val(); 
        data.action = "ajax_edit_profile";
        jQuery.post(ajaxurl,data,function(response){
        

        }).done(function() {
            $('#submit').prop('disabled', false);  
            $('#loading').slideUp( 300 ).delay( 200 ).fadeIn( 200 ).html('Done'); 
        }); 

     });  


     $('.active-account').click(function(){
        
        var data={}; 
        var id = $(this).attr('id');
        var goal  = $(this).attr('data');
        var target  = $(this).attr('target');

      /******* create blank page ******/
        $('.loading'+id).show().html('<span class="spinner is-active"></span>'); 
        $('.submit'+id).prop('disabled', true);

        data.uid        = id;
        data.goal       = goal;
        data.target     = target;  
        data.action = "ajax_save_enable";
        jQuery.post(ajaxurl,data,function(response){
         //$('.result').append(response);
        
        if( response == 0) { $('#'+id).removeClass('uk-button-danger').addClass('uk-button-success').html('Active').attr('data', 1); } 
        if( response == 1) { $('#'+id).removeClass('uk-button-success').addClass('uk-button-danger').html('Disable').attr('data', 0); }

        }).done(function() {
            $('.submit'+id).prop('disabled', false); 
            $('.loading'+id).slideUp( 300 ).delay( 200 ).fadeIn( 200 ).html('Done'); 
        }); 

     });    
  
    /* google search*/
    $('#salmon-search').click(function(){
        
        var data={}; 

      /******* create blank page ******/
        $('#loading').show().html('<span class="spinner is-active"></span>'); 
        $('#submit').prop('disabled', true);
        
        $('.result tr').html('');

        data.limit        =   $('#salmon-limit').val();
        data.sources      =   $('#salmon-source').val();
        data.keyword      =   $('#salmon-keyword').val(); 
        data.action = "ajax_salmon_search";
        jQuery.post(ajaxurl,data,function(response){
             
          var html;
          var related = '<b>Related keyword:</b> ';

          if (undefined !== response.title && response.title.length) {
          for (var i = 0; i < response.title.length; i++) {
              //console.log(response.title[i]+'='+response.url[i]);

              ///var basename = response.title[i].replace(/[^A-Z0-9]/ig, "_");
              html += '<tr><td style="max-width:220px; word-wrap: break-word;">'+'<b>'+response.title[i]+'</b>'+'<br><a href="'+response.url[i]+'">'+response.url[i]+'</a></td>' +
                    '<td><i class="uk-icon-facebook-square"></i> <span class="fb'+i+'"></span></td>' +
                    '<td><i class="uk-icon-google-plus"></i> <span class="google'+i+'"></span></td>' + 
                    '<td><i class="uk-icon-pinterest"></i> <span class="pinterest'+i+'"></span></td>' + 
                    '<td><i class="uk-icon-linkedin-square"></i> <span class="linkedin'+i+'"></span></td>' + 
                    '<td><span class="total'+i+'"></span></tr>' +
                    '<tr><td colspan="6"><div class="uk-grid">' +
                    '<div class="uk-width-1-1"><div class="uk-width-1-1">' +
                    '<label>Css Selector &nbsp;</label><input type="text" class="uk-width-1-3 salmon-css-selector" data="'+response.url[i]+'" id="'+i+'">' +
                    '<button type="button" data="'+response.url[i]+'" id="'+i+'" class="uk-float-right uk-button uk-button-small uk-button-success salmon-curate"> <i class="uk-icon-plus-circle"></i> Curate</button>' +
                    '<span class="loading'+i+'"></span><button type="button" class="uk-button uk-button-success uk-button-small salmon-insert-post" id="'+i+'">Insert Post</button></div></div>' +
                    '<div class="uk-width-1-1"><div class="editable'+i+'" id="salmon-preview'+i+'"></div></div><script> var editor = new MediumEditor(".editable'+i+'");</script></div></td>' +
                    '</tr>';     
              
              doStuff( response.url[i], i )
          };
          }

          if (undefined !== response.related_url && response.related_url.length) {
          for (var y = 0; y < response.related_url.length; y++) {
              //console.log(response.related_url[y]);
              related += response.related_url[y]+', ';
          };
          }

          $('.result tr:first').after(html);  
          $('#salmon-related').html(related);

        }).done(function() {
            $('#submit').prop('disabled', false); 
            $('#loading').slideUp( 300 ).delay( 200 ).fadeIn( 200 ).html(''); 
        }); 

    });
  
    /* popup */
    $('.salmon-curate').live('click',function(){
      var url = $(this).attr('data');
      var id = $(this).attr('id');
      //alert('ss'+id);
      //var modal = UIkit.modal(".uk-modal");
      //modal.show();
      //$('.datas').val(id);
      //$('.title').html(id);
      $('.loading'+id).show().html('<span class="spinner is-active"></span>'); 
      var data={}; 
      var url = url;
      var css = '';

      data.url  = url;
      data.css  = css;  
      data.action = "ajax_salmon_curate";
      jQuery.post(ajaxurl,data,function(response){
          
          $('.loading'+id).show().html('<span class="spinner">..................</span>'); 
          $("#salmon-preview"+id).html( response ); 

      }).done(function() {  
        
      }); 


    });

    /* selecttor field */
    $(".salmon-css-selector").live('keypress',function(event) {
      
       
      if (event.which == 13) {
         
         var url = $(this).attr('data');
      var id = $(this).attr('id');

      $('.loading'+id).show().html('<span class="spinner is-active"></span>'); 
      var data={}; 
      var url = url;
      var css = $(this).val();

      data.url  = url;
      data.css  = css;  
      data.action = "ajax_salmon_curate";
      jQuery.post(ajaxurl,data,function(response){
          
          $('.loading'+id).show().html('<span class="spinner">..................</span>'); 
          $("#salmon-preview"+id).html( response ); 

      }).done(function() {  
        
      }); 
          
      return false;
      }

    });



    $('.salmon-insert-post').live('click',function(){
      
      var id = $(this).attr('id');
      var html =  $("#salmon-preview"+id).html(); 
      appendText( html ); 

    });

    $('#salmon-insert-post').live('click',function(){
      //tinyMCE.get('mycustomeditor').getContent();
     
      //var modal = UIkit.modal(".uk-modal");
      //modal.hide();

       var s = 'ss';
      //appendText('dddd'); 

    });

    function appendText(text) {
      //Insert content
      parent.tinyMCE.activeEditor.setContent(parent.tinyMCE.activeEditor.getContent() + text);
      //Close window
      parent.jQuery("#TB_closeWindowButton").click();
    }



    var timesRun = 0;
    var startTime = new Date().getTime();
    var doStuff = function (video_id, i) {   

      $.ajax({
                    url: 'https://count.donreach.com/?url='+video_id,
                    type: "GET",
                    cache:false, 
                    data : { post: video_id }, 
                    beforeSend: function( xhr ) { 
                    },
                    success : function(data){     
                      
                      var pinterest = data['shares']['pinterest'];
                      var linkedin = data['shares']['linkedin'];

                      $('.fb'+i).html(data['shares']['facebook']);
                      $('.google'+i).html(data['shares']['google']);
                      $('.pinterest'+i).html(pinterest);
                      $('.linkedin'+i).html(linkedin); 
                      $('.total'+i).html(data['total']); 
                      
                    }
      });  
    };  

    /* Editor */
   

  });

}(jQuery));


   