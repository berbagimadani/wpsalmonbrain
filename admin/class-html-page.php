
<?php

class Html_Page {
    
    /**
    * Render php html input
    */
    public function render_page_one( $plugin_option, $file ) {

    	require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/inc/class-wpsocialguardian-field.php'; 
		$gt = new Wpsocialguardian_Field(); 

        $options = get_option( $plugin_option );
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/'.$file.'.php';
    }

}

?>
