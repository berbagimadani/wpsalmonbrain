<?php

use Serps\SearchEngine\Google\GoogleClient;
use Serps\HttpClient\CurlClient;
use Serps\Core\Browser\Browser;
use Serps\SearchEngine\Google\GoogleUrl; 
use Serps\SearchEngine\Google\NaturalResultType; 

class Save_Option {
    
    /**
    * Render php html input
    */
    public function saveAppKey( $app_name, $app_id, $app_secret ) {

    	ob_clean();
 		global $wpdb;

 		echo $app_name;

	    $wpdb->insert( 
            $wpdb->prefix.'heronative_fb_account', 
            array( 
                'name_apps' => $app_name, 
                'app_id' => $app_id,
                'app_secret' =>  $app_secret
            ), 
            array( 
                '%s', 
                '%s',
                '%s', 
            ) 
        );
	    echo '<tr><td>'.$app_name.'</td><td>'.$app_id.'</td><td></td><td></td><td><a href="'.CALL_BACK.'page=wpsalmonbrain-socialmedia&type=fbprofile">PLease Refresh</a></td></tr>';
	    wp_die();

    }

    /*
    **
    * Search engine serp google or another
    */
    public function salmonSearch( $limit, $sources, $keyword) {

        ob_clean(); 


        $userAgent = 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.93 Safari/537.36';
        $language = 'en-US,en;q=0.8,ID';

        $browser = new Browser(new CurlClient(), $userAgent, $language, $someCookies=null, $proxy=null);
        $googleUrl = new GoogleUrl( $sources );

        $googleClient = new GoogleClient($browser);
        $googleUrl->setSearchTerm( $keyword );
        $googleUrl->setResultsPerPage($limit); 
    

         $response = null;

        try{
            
            $response = $googleClient->query($googleUrl, $browser);
        }catch(HttpResponseErrorException $e){
            // Http response is not valid, maybe an error from google or your url
            $errorInfo = $e->getMessage();
        }catch(CaptchaException $e){
            // Captcha needs to be solved

            print_r($e);
        }catch(RequestErrorException $e){
            // Other request error are handled here, maybe something wrong with your network
            $errorInfo = $e->getMessage();

            print_r($errorInfo);
        }

       

        if($response){
            try{
                $results = $response->getNaturalResults();
                $datas = [];
                foreach($results as $result){
                    // parse results
                     //echo $result->title."<br>";
                    //echo '<tr><td style="max-width:220px">'.'<b>'.$result->title.'</b>'.'<br>'.$result->url.'</td><td>'.$sources.'</td><td>'.$keyword.'</td><td><button type="button" data='.$keyword.' class="uk-button uk-button-small uk-button-success salmon-curate"> <i class="uk-icon-plus-circle"></i> Curate</button></td></tr>';
                    
                    $datas['title'][] = $result->title;
                    $datas['url'][] = $result->url;

                }

                $relatedSearches = $response->getRelatedSearches();
                foreach ($relatedSearches as $relatedSearch) { 
                  $datas['related_url'][] = $relatedSearch->title;
                }

                wp_send_json($datas);
                

            }catch(InvalidDOMException $e){
                // Something bad happened while parsing
                // Maybe an update of the library is needed, 
                // the exception message will tell more about
                $errorInfo = $e->getMessage();
            }
        }


        wp_die();

    }

    /*
    **
    * --------------
    */
    public function salmonCurate( $url, $css ) {

        ob_clean(); 

        
        $html = new simple_html_dom();

        $url = $url;
        $web = new WebBrowser();
        $result = $web->Process($url);

        if (!$result["success"])  echo "Error retrieving URL.  " . $result["error"] . "\n";
        else if ($result["response"]["code"] != 200)  echo "Error retrieving URL.  Server returned:  " . $result["response"]["code"] . " " . $result["response"]["meaning"] . "\n";
        else
        {
          //echo "".$css;

          $html->load($result["body"]);
          $rows = $html->find(!empty($css) ? $css : 'body', 0)->innertext; 

          print_r($rows);
        }
         

        wp_die();

    }



    /*
    **
    * --------------
    */

}

?>
