<?php
ob_start();
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       kabarharian.com
 * @since      1.0.0
 *
 * @package    Wpsalmonbrain
 * @subpackage Wpsalmonbrain/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wpsalmonbrain
 * @subpackage Wpsalmonbrain/admin
 * @author     Ade iskandar <berbagimadani@gmail.com>
 */

$http_host = !empty($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
$request_uri = !empty($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';
$hostname= 'http://'.$http_host . dirname(strtok($request_uri,'?'));
define('CALL_BACK', $hostname.'/admin.php?page='.@$_GET['page'].'&');

class Wpsalmonbrain_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */

	private $plugin_slug;
	private $plugin_option;

	protected $plugin_screen_hook = array(); 

	public function __construct( $plugin_name, $version, $plugin_slug, $plugin_option ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->plugin_slug = $plugin_slug;
		$this->plugin_option = $plugin_option;

		// register actions
		add_action( 'admin_menu', array( $this, 'wpsalmonbrain_admin_menu' ) );

		/* Save OPtion */
		add_action( 'admin_init', array( $this, 'register_wpsalmonbrain' ) );
		add_action( 'admin_notices', array( $this, 'unique_identifyer_admin_notices' ) );
		 
		add_action( 'wp_ajax_ajax_save_profile', array( $this, 'ajax_save_profile') );
		add_action( 'wp_ajax_nopriv_ch_post_action', array( $this, 'ajax_save_profile') ); 
		/*........*/
		add_action( 'wp_ajax_ajax_salmon_search', array( $this, 'ajax_salmon_search') );
		add_action( 'wp_ajax_nopriv_ch_post_action', array( $this, 'ajax_salmon_search') );
		/*........*/
		add_action( 'wp_ajax_ajax_salmon_curate', array( $this, 'ajax_salmon_curate') );
		add_action( 'wp_ajax_nopriv_ch_post_action', array( $this, 'ajax_salmon_curate') ); 

	}	


	/**
	* Register validation option
	*/
	public function register_wpsalmonbrain() {
		register_setting( $this->plugin_option, $this->plugin_option, array( $this, 'gt_validate_options' ) );    
	} 


	public function ajax_save_profile() { 

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-save-option.php';
		$app_name  = $_POST['name_apps'];
 		$app_id	= $_POST['app_id'];
 		$app_secret = $_POST['app_secret'];

		$act = new Save_Option();
		$act->saveAppKey( $app_name, $app_id, $app_secret );

	}
	
	/* ............. */

	public function ajax_salmon_search() { 

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-save-option.php';
		$limit  = $_POST['limit'];
 		$sources	= $_POST['sources'];
 		$keyword = $_POST['keyword'];

		$act = new Save_Option();
		$act->salmonSearch( $limit, $sources, $keyword );
	}

	/* ............. */

	public function ajax_salmon_curate() { 

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-save-option.php';
		$url  = $_POST['url'];
 		$css	= $_POST['css']; 

		$act = new Save_Option();
		$act->salmonCurate( $url, $css);
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		if ( !isset( $this->plugin_screen_hook ) ) {
			return;
		}

		$screen = get_current_screen();
		if ( $screen->id == 'post' || $this->plugin_screen_hook[0] == $screen->id || @$this->plugin_screen_hook[1] == $screen->id || @$this->plugin_screen_hook[2] == $screen->id || @$this->plugin_screen_hook[3] == $screen->id || @$this->plugin_screen_hook[4] == $screen->id || @$this->plugin_screen_hook[5] == $screen->id) {
			
			wp_enqueue_style( $this->plugin_slug . '-uikit-styles', plugins_url( 'css/uikit.min.css', __FILE__ ), array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_slug . '-base-styles', plugins_url( 'css/base.css', __FILE__ ), array(), $this->version, 'all' );
			wp_enqueue_style( $this->plugin_slug . '-date-styles', plugins_url( 'css/datepicker.min.css', __FILE__ ), array(), $this->version, 'all' );

			wp_enqueue_style( $this->plugin_slug . '-pop', ('https://daankuijsten.github.io/simple-popup/dist/jquery.simple-popup.min.css'), array(), null );

			wp_enqueue_style( $this->plugin_slug . '-popsss', ('//cdn.jsdelivr.net/medium-editor/latest/css/medium-editor.min.css'), array(), null );

			
			wp_enqueue_style( 'wp-color-picker' );

			//wp_register_style('jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css');
  			//wp_enqueue_style( 'jquery-ui' ); 
			
		}

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		if ( !isset( $this->plugin_screen_hook ) ) {
			return;
		}
		global $post;
		$screen = get_current_screen(); 

		if ( $screen->id == 'post' || $this->plugin_screen_hook[0] == $screen->id || @$this->plugin_screen_hook[1] == $screen->id || @$this->plugin_screen_hook[2] == $screen->id || @$this->plugin_screen_hook[3] == $screen->id ) {
			
			wp_enqueue_script('media-upload');
			wp_enqueue_style('thickbox');
			wp_enqueue_script( $this->plugin_slug . '-uikit-script', plugins_url( 'js/uikit.min.js', __FILE__ ), array( 'jquery', 'jquery-ui-tabs', 'wp-color-picker' ), $this->version );
			wp_enqueue_script( $this->plugin_slug . '-core-script', plugins_url( 'js/core.js', __FILE__ ), array( 'jquery' ), $this->version );
			wp_enqueue_script( $this->plugin_slug . '-custome-script', plugins_url( 'js/custome.js', __FILE__ ), array( 'jquery' ), $this->version );
			wp_enqueue_script( $this->plugin_slug . '-media-upload-script', plugins_url( 'js/media-upload.js', __FILE__ ), array( 'jquery' ), $this->version );

			/* component */ 
			wp_enqueue_script( $this->plugin_slug . '-date-script', ( 'http://getuikit.com/src/js/components/datepicker.js' ), array( 'jquery' ), $this->version );
			
			wp_enqueue_script( $this->plugin_slug . '-simple-popup', ( 'https://daankuijsten.github.io/simple-popup/dist/jquery.simple-popup.min.js' ), array( 'jquery' ), null );
  			
  			wp_enqueue_script( $this->plugin_slug . '-editor1', ( '//cdn.jsdelivr.net/medium-editor/latest/js/medium-editor.min.js' ), array( 'jquery' ), null );
  			//wp_enqueue_script( $this->plugin_slug . '-editor2', ( 'http://michelson.github.io/Dante/assets/javascripts/deps.js' ), array( 'jquery' ), null );
  			//wp_enqueue_script( $this->plugin_slug . '-editor3', ( 'https://daankuijsten.github.io/simple-popup/dist/jquery.simple-popup.min.js' ), array( 'jquery' ), null );

			//add_action( 'admin_print_scripts', array( $this, 'admin_inline_js' ) );			  

		    wp_enqueue_media();
		}

	}


	public function wpsalmonbrain_admin_menu(){  
	 
		$this->plugin_screen_hook[] = add_menu_page( __( 'Wp Salmon brain', 'Page Title' ), 'Salmon Brain', 'administrator', $this->plugin_slug, array( $this, 'display_plugin_admin_guardian' ), 'dashicons-megaphone' );
		$this->plugin_screen_hook[] = add_submenu_page( $this->plugin_slug, __( 'Social Media', 'Sub Title' ), 'Social Media', 'manage_options', $this->plugin_slug.'-socialmedia',  array( $this, 'display_plugin_admin_social_media' ) ); 
		$this->plugin_screen_hook[] = add_submenu_page( $this->plugin_slug, __( 'Logs', 'Sub Title' ), 'Logs', 'manage_options', $this->plugin_slug.'-logs',  array( $this, 'display_plugin_admin_page_log' ) );
		$this->plugin_screen_hook[] = add_submenu_page( $this->plugin_slug, __( 'Settings', 'Sub Title' ), 'Settings', 'manage_options', $this->plugin_slug.'-settings',  array( $this, 'display_plugin_admin_page_setting' ) );
 
 	}

 	public function display_plugin_admin_guardian() {  

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-html-page.php';
		$view = new Html_Page();
		echo $view->render_page_one( $this->plugin_option, 'salmonbrain' );
	}
 	public function display_plugin_admin_social_media() {

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-html-page.php';
		$view = new Html_Page();
		echo $view->render_page_one( $this->plugin_option, 'social-media' ); 

	}  
	public function display_plugin_admin_page_log() {
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-html-page.php';
		$view = new Html_Page();
		echo $view->render_page_one( $this->plugin_option, 'log' ); 
	}
 	public function display_plugin_admin_page_setting() {

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-html-page.php';
		$view = new Html_Page();
		echo $view->render_page_one( $this->plugin_option, 'setting' ); 
	}


	public function _show_msg( $message, $msgclass = 'info' ){
		echo "<div id='message' class='$msgclass'>$message</div>";
	}
	public function gt_validate_options( $input ) {
		
		/*if ( isset( $input['campaign'] ) ) {
			$valid['campaign'] = wp_filter_post_kses( $input['campaign'] );
		} */

		return apply_filters( 'gt_validate_options', $input );  

	}
	public function unique_identifyer_admin_notices() {
		
		if(isset($_GET['page'])){
		
			$genthemes_settings_pg = strpos( $_GET['page'], $this->plugin_option.'-theme' );
			$set_errors = get_settings_errors(); 
	
			if(current_user_can ('manage_options') && $genthemes_settings_pg !== FALSE && !empty($set_errors)){ 
				
				if ($set_errors[0]['code'] == 'settings_updated' && isset( $_GET['settings-updated']) ){
					$this->_show_msg("<p>" . $set_errors[0]['message'] . "</p>", 'updated');
				}else{
					// there maybe more than one so run a foreach loop.
					foreach($set_errors as $set_error){
						// set the title attribute to match the error "setting title" - need this in js file
						wptuts_show_msg("<p class='setting-error-message' title='" . $set_error['setting'] . "'>" . $set_error['message'] . "</p>", 'error');
					}
				}
			}

		}
	}

}
