<script language="javascript" type="text/javascript">
function popWindow(url,winName,w,h) {
    if (window.open) {
        if (poppedWindow) { poppedWindow = ''; }
        //GET SIZE OF WINDOW/SCREEN
        windowW = w;
        windowH = h;
        var windowX = (screen.width/2)-(windowW/2);
        var windowY = (screen.height/2)-(windowH/2);
        var myExtra = "status=no,menubar=no,resizable=yes,toolbar=no,scrollbars=yes,addressbar=no";
        var poppedWindow = window.open(url,winName,'width='+w+',height='+h+',top='+windowY+',left=' + windowX + ',' + myExtra + '');
        setTimeout(refreshThis,3000);
    }
    else {
        alert('Your security settings are not allowing our popup windows to function. Please make sure your security software allows popup windows to be opened by this web application.');
    }
}
</script>
<?php  $options = get_option( $plugin_option );  ?> 
<?php if( @$_GET['act'] == '' ) : ?>

<div class="">
    <div class="uk-grid">
 
        <div class="uk-width-1-1">
            <div class="uk-panel uk-panel-box box-guardian">  
                
                <!-- This is a button toggling the modal --> 
                <button class="uk-button uk-button-success" data-uk-modal="{target:'#form-new', center:true}"> <i class="uk-icon-plus-circle"></i> New APP ID</button>

                <!-- FORM  -->
                <div id="form-new" class="uk-modal">
                <div class="uk-modal-dialog">
                <a class="uk-modal-close uk-close"></a>

                <div class="uk-margin-top"> 
                    <form class="uk-form uk-form-stacked uk-margin-top"> 
                        
                        <div class="uk-form-row">
                            <label class="uk-form-label" for="form-s-it">Name APP ID</label>
                            <div class="uk-form-controls"> 
                                <?php 
                                $gt->field( array(
                                    'type'          => 'text', 
                                    'name'          => $plugin_option.'-name-apps', 
                                    'class'         => 'uk-width-1-1 uk-form-large',
                                    'default'       => !empty($options['name-apps']) ? $options['name-apps'] : '', 
                                    'placeholder'   => ''
                                ));
                                ?>
                            </div>
                        </div> 
                        <div class="uk-form-row">
                            <label class="uk-form-label" for="form-s-it">App ID </label>
                            <div class="uk-form-controls"> 
                                <?php 
                                $gt->field( array(
                                    'type'          => 'text', 
                                    'name'          => $plugin_option.'-fb-app-id', 
                                    'class'         => 'uk-width-1-1',
                                    'default'       => !empty($options['fb-app-id']) ? $options['fb-app-id'] : '', 
                                    'placeholder'   => ''
                                ));
                                ?>
                            </div>
                        </div> 
                        <div class="uk-form-row">
                            <label class="uk-form-label" for="form-s-it">App Secret </label>
                            <div class="uk-form-controls"> 
                                <?php 
                                $gt->field( array(
                                    'type'          => 'text', 
                                    'name'          => $plugin_option.'-fb-app-secret', 
                                    'class'         => 'uk-width-1-1',
                                    'default'       => !empty($options['fb-app-secret']) ? $options['fb-app-secret'] : '', 
                                    'placeholder'   => ''
                                ));
                                ?>
                            </div>
                        </div> 

                        <div class="uk-form-row"> 
                            <button type="button" class="uk-button button-primary uk-button-large uk-width-1-1" id="save-profile"> <span id="loading"> </span> Save </button>
                        </div>
                    </form>
                </div>

                </div>
                </div>

                <!-- End FORM -->

            </div>
        </div> 
          <!-- This is a button toggling the modal 
            <button class="uk-button tes" data-uk-modal="{target:'#my-id'}">...</button>
             
            <div id="my-id" class="uk-modal">
                <div class="uk-modal-dialog">
                    <a class="uk-modal-close uk-close"></a>
                    <textarea data-uk-htmleditor="{mode:'tab'}" class="co">xx</textarea>
                </div>
            </div> -->
        <div class="uk-width-1-1 uk-margin-top pagination">
            <table class="uk-table uk-table-striped result" style="background:#fff">
                <tr>
                    <th>Name APP ID</th>
                    <th>App Id</th>
                    <th>UID</th>
                    <th>My Profile</th> 
                    <th colspan="2">Action</th>
                </tr>
               
                <?php
                global $wpdb; 
                $comments_per_page = 30;
                $offset = 0;
                if(isset($_GET['cpage']) && !empty($_GET['cpage'])) {
                    $offset =  ($_GET['cpage']-1) * $comments_per_page; // (page 2 - 1)*10 = offset of 10
                }
                $myrows = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}heronative_fb_account order by id desc LIMIT $offset,$comments_per_page" );
                foreach ($myrows as $key => $value) {
                ?>
                <tr>
                    <td>
                        <?php echo $value->name_apps; ?> 
                    </td>
                    <td><?php echo $value->app_id; ?></td>
                    <td><?php echo $value->uid; ?></td>
                    <td>
                        <a href="http://facebook.com/<?php echo $value->uid; ?>" target="_blank"><?php echo $value->full_name; ?></a>
                    </td>
                    <td>
                        <?php if(empty($value->uid)) { ?>
                        <a href="#" class="uk-button uk-button-small" onclick="return popWindow('<?php echo CALL_BACK.'act=login&id='.$value->id;?>&type=fbprofile','myPoppedWindowName','900','500');">
                            <i class="dashicons dashicons-facebook-alt"></i> Please Login into Facebook
                        </a>
                        <?php } ?>

                        <a href="<?php echo CALL_BACK.'act=post&id='.$value->id;?>&type=fbprofile" class="uk-button uk-button-primary uk-button-mini">
                            <i class="fa fa-facebook"></i> Test
                        </a>
                    </td>
                    <td>
                         <?php if($value->status == 1) {?>
                        <button class="uk-button uk-button-mini uk-button-danger active-account" id="<?php echo $value->id?>" data="0" target="fbprofile">Disable</button>
                        <?php } ?>
                        <?php if($value->status == null) {?>
                        <button class="uk-button uk-button-mini uk-button-success active-account" id="<?php echo $value->id?>" data="1" target="fbprofile">Active</button>
                        <?php } ?>
                        <span class="loading<?php echo $value->id; ?>"></span>

                        <a href="<?php echo CALL_BACK.'act=edit&id='.$value->id;?>&type=fbprofile" class="uk-button uk-button-mini uk-button-danger">Edit</a>

                        <a href="<?php echo CALL_BACK.'act=delete&id='.$value->id.'&type=fbprofile'; ?>" onclick="return confirm('Are you sure delete?')" class="uk-icon-hover uk-icon-remove"> </a>
                        &nbsp;
                        <a href="<?php echo CALL_BACK.'act=reset&id='.$value->id.'&type=fbprofile'; ?>" onclick="return confirm('Are you sure reset?')" class="uk-icon-hover uk-icon-eraser"> </a>
                    </td>
                </tr>
                <?php } ?> 
            </table>

            <?php
                $total = $wpdb->get_var("
                    SELECT COUNT(id)
                    FROM {$wpdb->prefix}heronative_fb_account 
                ");
                $page = isset( $_GET['cpage'] ) ? abs( (int) $_GET['cpage'] ) : 1;

                echo paginate_links( array(
                    'base' => add_query_arg( 'cpage', '%#%' ),
                    'format' => '',
                    'prev_text' => __('&laquo;'),
                    'next_text' => __('&raquo;'),
                    'total' => ceil($total / $comments_per_page),
                    'current' => $page
                ));
            ?>

        </div>
 

    </div>
</div>
<?php endif; ?>

<?php
if( @$_GET['act'] == 'edit' ) {
    global $wpdb;
    $row = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}heronative_fb_account WHERE id = {$_GET["id"]}" );
?>
    <div class="uk-grid">
        <div class="uk-width-1-2">
            <div class="uk-panel uk-panel-box">
                <h4 class="tm-article-subtitle">Facebook APP Key & Profile</h4>
            <div class="uk-grid-">
                <form class="uk-form uk-form-horizontal"> 
                    <div class="uk-form-row">
                        <input type="hidden" id="<?php echo $plugin_option.'-id';?>" value="<?php echo $row->id;?>">
                        <input type="hidden" id="redirect" value="<?php echo CALL_BACK.'&type=fbprofile';?>">
                        <label class="uk-form-label" for="form-s-it">Name APPS or Profile</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'text', 
                                'name'          => $plugin_option.'-name-apps', 
                                'class'         => 'uk-width-1-1',
                                'default'       => $row->name_apps, 
                                'placeholder'   => ''
                            ));
                            ?>
                        </div>
                    </div> 
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">App ID </label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'text', 
                                'name'          => $plugin_option.'-fb-app-id', 
                                'class'         => 'uk-width-1-1',
                                'default'       => $row->app_id, 
                                'placeholder'   => ''
                            ));
                            ?>
                        </div>
                    </div> 
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">App Secret </label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'text', 
                                'name'          => $plugin_option.'-fb-app-secret', 
                                'class'         => 'uk-width-1-1',
                                'default'       => $row->app_secret, 
                                'placeholder'   => ''
                            ));
                            ?>
                        </div>
                    </div> 

                    <div class="uk-form-row">
                        <div class="uk-width-1-2">
                        <button type="button" class="uk-button button-primary" id="edit-profile">Save </button><span id="loading"></span>
                        <a href="<?php echo CALL_BACK.'&type=fbprofile'; ?>" class="uk-button uk-button-primary">BACK</a>
                        </div>
                    </div>
               </form>
            </div>
            </div>
        </div>
    </div>

<?php } ?>

<?php 
/* delete account */
if( @$_GET['act'] == 'delete' ) {
    global $wpdb;
    $wpdb->delete( $wpdb->prefix.'heronative_fb_account', array( 'id' => @$_GET['id'] ) );
    wp_redirect(CALL_BACK.'&type='.@$_GET['type']);
}  

/* reset account */
if( @$_GET['act'] == 'reset' ) {
    global $wpdb;
    $wpdb->update( 
    $wpdb->prefix.'heronative_fb_account', 
        array( 
            'full_name' => '', 
            'uid' => '',
            'access_token' =>  ''
        ),
        array('id' => $_GET["id"]),
        array( '%s', '%s', '%s' ),
        array( '%d' ) 
    );
    wp_redirect(CALL_BACK.'&type=fbprofile');
} 

if( @$_GET['act'] == 'login' ) {
    global $wpdb;

    function loginFacebook($wpdb) {
        $row = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}heronative_fb_account WHERE id = {$_GET["id"]}" );
        $fb = new Facebook\Facebook([
            'app_id' => $row->app_id,
            'app_secret' => $row->app_secret,
            'default_graph_version' => 'v2.4',
        ]);
        $helper = $fb->getRedirectLoginHelper();
        $permissions = [ 'email', 'user_friends', 'manage_pages', 'publish_actions', 'publish_pages', 'user_posts', 'read_insights'  ]; // optional
        $loginUrl = $helper->getLoginUrl(CALL_BACK.'act=update&type=fbprofile&id='.$_GET["id"], $permissions);
        $request_to = $loginUrl;
        wp_redirect( $loginUrl );
        exit;
    }

    function checkFacebookAccount($wpdb) { ;
        $user_count = $wpdb->get_row( "SELECT uid FROM {$wpdb->prefix}heronative_fb_account WHERE id = {$_GET["id"]}" );
        if ( @$user_count->uid > 0 ) {  
            //wp_redirect(CALL_BACK.'act=page&id='.$_GET["id"]);
            wp_redirect(CALL_BACK.'&type=fbprofile');
            //echo 'xx';
        } else { 
            loginFacebook($wpdb);
            //echo 'zzz';  
        }
    }
    checkFacebookAccount($wpdb);
} 

/* after login insert database*/
if( @$_GET['act'] == 'update' ) {

    global $wpdb;

    $row = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}heronative_fb_account WHERE id = {$_GET["id"]}" );
    $fb = new Facebook\Facebook([
        'app_id' => $row->app_id,
        'app_secret' => $row->app_secret,
        'default_graph_version' => 'v2.4',
    ]);
    $helper = $fb->getRedirectLoginHelper();

    try {
        $accessToken = $helper->getAccessToken();
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // There was an error communicating with Graph
        echo $e->getMessage();
        exit;
    }

    if (isset($accessToken)) {
        // User authenticated your app!
        // Save the access token to a session and redirect
        $_SESSION['facebook_access_token'] = (string) $accessToken;
        // Log them into your web framework here . . .
        $response = $fb->get('/me', $_SESSION['facebook_access_token']);
        $node = $response->getGraphNode();
        $wpdb->update( 
        $wpdb->prefix.'heronative_fb_account', 
        array( 
            'full_name' => $node['name'], 
            'uid' => $node['id'],
            'access_token' =>  $_SESSION['facebook_access_token']
            ), 
            
            array('id' => $_GET["id"]),
            array( '%s', '%s', '%s' ),
            array( '%d' ) 
        );

        wp_redirect(CALL_BACK.'act=page&id='.$_GET["id"].'&type=fbprofile');
    }   
}

/* view pages */
if( @$_GET['act'] == 'page' ) {

    global $wpdb;

    $row = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}heronative_fb_account WHERE id = {$_GET["id"]}" );
    $fb = new Facebook\Facebook([
        'app_id' => $row->app_id,
        'app_secret' => $row->app_secret,
        'default_graph_version' => 'v2.4',
    ]); 
     $access_token = $row->access_token;
    // Sets the default fallback access token so we don't have to pass it to each request
    $fb->setDefaultAccessToken($access_token);

    try { 
        $response = $fb->get('/me/accounts?limit=20000');  
        $me = $fb->get('/me');  

    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        // When Graph returns an error
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
    // When validation fails or other local issues
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
    exit;
    }
        
    $plainOldArray = $response->getDecodedBody();
    $node = $me->getGraphNode();

    $data = array(
        'profile'   => $node['name'],
        'my_pages'  => $plainOldArray
    );
        
    //print_r($data['my_pages']);
    //wp_redirect(CALL_BACK);
    ?>

    <script>
            window.setInterval(function(){
            window.opener.location.href="<?php echo CALL_BACK.'&type=fbprofile'; ?>";
                self.close(); 
            }, 1000);                   
            </script>

    <?php
}


/* post test */
if( @$_GET['act'] == 'post' ) {

    global $wpdb;

    $row = $wpdb->get_row( "SELECT * FROM {$wpdb->prefix}heronative_fb_account WHERE id = {$_GET["id"]}" );
    $fb = new Facebook\Facebook([
        'app_id' => $row->app_id,
        'app_secret' => $row->app_secret,
        'default_graph_version' => 'v2.4',
    ]); 
    $access_token = $row->access_token;

    $post_body = array( 
        'message'       => 'Plugins Wordpress Auto Post Multiple Account http://wpguardian.co/',
        'link'          => '',
        'picture'       => '', 
        'description'   => '',
        'caption'       => '',
        'tags'          => ''
    );
    $msgParams = null;
    if(is_array($post_body)) :
        foreach ($post_body as $k => $parameter) {
          if ( !empty($parameter) || !is_null($parameter) ){ 
            $msgParams[$k]= $parameter;
          } 
        }
    endif;

    $result = null;
        try { 
        
        $page_id = $row->uid;

        $response = $fb->post('/'.$page_id.'/feed', $msgParams, $access_token ); 
        $result = $response->getDecodedBody();

        /*logs*/
        $wpdb->insert( 
            $wpdb->prefix.'heronative_logs', 
            array( 'full_name' => $row->full_name, 'uid' => $row->uid, 'details' => json_encode($result)), 
            array( '%s', '%s', '%s' )
        );
    
        echo '<br>';
        //print_r($result);
        echo '<br>';
        echo 'Success test posting your profile facebook <a href="'.CALL_BACK.'&type=fbprofile"> BACK </a>';

        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage(); 
            /*logs*/
            $wpdb->insert( 
                $wpdb->prefix.'heronative_logs', 
                array('full_name' => $row->full_name, 'uid' => $row->uid,'details' => $e->getMessage()), 
                array( '%s', '%s', '%s' )
            );

        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            /*logs*/
            $wpdb->insert( 
                $wpdb->prefix.'heronative_logs', 
                array('full_name' => $row->full_name,'uid' => $row->uid,'details' => $e->getMessage()), 
                array( '%s', '%s', '%s' )
            );
        }
}

?>