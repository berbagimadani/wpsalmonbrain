<div class="">
    <div class="uk-grid">
        <?php if( isset($_GET['settings-updated']) ) { ?>
        <div class="uk-width-1-1">
        <div id="message" class="updated">
                    <p><strong><?php _e('Settings saved') ?></strong></p>
        </div>
        </div>
        <?php } ?>

        <div class="uk-width-6-10 uk-margin-top"> 
            <a href="<?php echo CALL_BACK; ?>" class="uk-link-reset">
                <button class="uk-button uk-button-success label">Salmon Brain :: Settings</button>
            </a> 
        </div>

        <div class="uk-width-6-10">
            <div class="uk-panel uk-panel-box box-guardian"> 

                <form method="post" action="options.php" class="uk-form uk-form-stacked"> 
                <?php 
                settings_fields( $plugin_option ); 
                $options = get_option( $plugin_option ); 
                ?>
                    <!--<div class="uk-form-row">
                        <label class="uk-form-label label-guardian" for="form-s-it">* Auto Post Active</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'radio', 
                                'name'          => $plugin_option.'[auto_post]', 
                                'class'         => 'uk-width-1-2',
                                'default'       => !empty($options['auto_post']) ? $options['auto_post'] : '1', 
                                'options'       => array( '1' => 'Active', '2' => 'Non-Active' )
                            ));
                            ?>
                        </div>
                    </div>-->

                    <!--<div class="uk-form-row">
                        <label class="uk-form-label label-guardian" for="form-s-it">* Available Social Media</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'checkbox', 
                                'name'          => $plugin_option.'[social_media]', 
                                'class'         => 'uk-width-1-1',
                                'default'       => !empty($options['social_media']) ? $options['social_media'] : 'fbprofile', 
                                'options'       => array( 
                                    'fbprofile'     => 'Fb Profile',  
                                )
                            ));
                            ?>
                            <?php 
                            $gt->field( array(
                                'type'          => 'checkbox', 
                                'name'          => $plugin_option.'[social_media]', 
                                'class'         => 'uk-width-1-1',
                                'default'       => !empty($options['social_media']) ? $options['social_media'] : 'fanspage', 
                                'options'       => array( 
                                    'fanspage'     => 'fanspage',  
                                )
                            ));
                            ?>
                            <?php 
                            $gt->field( array(
                                'type'          => 'checkbox', 
                                'name'          => $plugin_option.'[social_media]', 
                                'class'         => 'uk-width-1-1',
                                'default'       => !empty($options['social_media']) ? $options['social_media'] : 'twitter', 
                                'options'       => array( 
                                    'twitter'     => 'twitter',  
                                )
                            ));
                            ?>
                        </div>
                    </div>-->

                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Token/ApiKey Bitly </label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'text', 
                                'name'          => $plugin_option.'[app-id-bitly]', 
                                'class'         => 'uk-width-1-1 uk-form-large',
                                'default'       => !empty($options['app-id-bitly']) ? $options['app-id-bitly'] : '', 
                                'placeholder'   => ''
                            ));
                            ?>
                        </div>
                    </div>
                    <h4 class="tm-article-subtitle">Twitter Api</h4>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">* Consumer Key </label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'text', 
                                'name'          => $plugin_option.'[consumer-key]', 
                                'class'         => 'uk-width-1-1 uk-form-large',
                                'default'       => !empty($options['consumer-key']) ? $options['consumer-key'] : '', 
                                'placeholder'   => ''
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">* Consumer Secret</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'text', 
                                'name'          => $plugin_option.'[consumer-secret]', 
                                'class'         => 'uk-width-1-2 uk-form-large',
                                'default'       => !empty($options['consumer-secret']) ? $options['consumer-secret'] : '', 
                                'placeholder'   => ''
                            ));
                            ?>
                        </div>
                    </div>  
                    <h4 class="tm-article-subtitle">General</h4>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Facebook content label</label>
                        <div class="uk-form-controls"> 
                            <?php 
$added ='%title%
%description%
%link%'; 
                            $gt->field( array(
                                'type'          => 'textarea', 
                                'name'          => $plugin_option.'[added-content]',  
                                'default'       => !empty($options['added-content']) ? $options['added-content'] : $added, 
                                'cols'          => 95,
                                'rows'          => 10
                            ));
                            ?>
                        </div>
                        <small>Each posting content can added aditionnal content or message </small>
                       
                    </div>

                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Twitter content label</label>
                        <div class="uk-form-controls"> 
                            <?php 
$added2 ='%link%'; 
                            $gt->field( array(
                                'type'          => 'textarea', 
                                'name'          => $plugin_option.'[added-content-twitter]',  
                                'default'       => !empty($options['added-content-twitter']) ? $options['added-content-twitter'] : $added2, 
                                'cols'          => 95,
                                'rows'          => 4
                            ));
                            ?>
                        </div>
                        <small>Each posting content can added aditionnal content or message </small>
                        <div>
                            <code>%title%</code> Post Title <br>
                            <code>%description%</code> Post Content <br>
                            <code>%link%</code> Post Link
                        </div>
                    </div>  

                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">Setup Type Share</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'radio', 
                                'name'          => $plugin_option.'[setup]', 
                                'class'         => 'uk-width-1-2',
                                'default'       => !empty($options['setup']) ? $options['setup'] : 'share', 
                                'options'       => array( 'native' => 'Share With Native (uploads photo to your facebook) &nbsp', 'share' => 'Share Link with caption, images, desciption' )
                            ));
                            ?>
                        </div>
                    </div>

                   

                    <div class="uk-form-row">
                        <div class="uk-width-2-10">
                        <input type="submit" class="uk-button button-primary" value="Save"> <span id="loading"></span>
                        </div>
                    </div>
               </form>
            </div>
        </div>

        
    </div>
</div>

