<div class="">
    <div class="uk-grid">
    	<?php if( isset($_GET['settings-updated']) ) { ?>
        <div class="uk-width-1-1">
        <div id="message" class="updated">
                    <p><strong><?php _e('Settings saved. please update permalinks click Settings -> permalinks choose post name and Save Change') ?></strong></p>
        </div>
        </div>
        <?php } ?>

        <div class="uk-width-1-1 uk-margin-top">
            <h2 id="usage"><a href="<?php echo CALL_BACK; ?>" class="uk-link-reset">Logs</a></h2>
             <a href="<?php echo CALL_BACK.'act=clear'?>" onclick="return confirm('Are you sure clear logs?')">Clear Log </a>
        </div> 
        <?php
        if(@$_GET['act'] == 'clear') {
            global $wpdb; 
            $wpdb->query("TRUNCATE TABLE {$wpdb->prefix}heronative_logs");
            wp_redirect(CALL_BACK);
        }
        ?>
        <div class="uk-width-1-1">
            <div class="uk-panel uk-panel-box pagination"> 
                <table class="uk-table uk-table-striped result" style="background:#fff">
                <tr>
                    <th>Post Id (Wordpress)</th>
                    <th>Account</th>
                    <th>Details</th> 
                </tr>
               
                <?php
                global $wpdb; 
                $comments_per_page = 50;
                $offset = 0;
                if(isset($_GET['cpage']) && !empty($_GET['cpage'])) {
                    $offset =  ($_GET['cpage']-1) * $comments_per_page; // (page 2 - 1)*10 = offset of 10
                }
                $myrows = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}heronative_logs  order by id desc LIMIT $offset,$comments_per_page" );
                foreach ($myrows as $key => $value) {
                ?>
                <tr>
                    <td>( <?php echo $value->post_id; ?> ) <a href="http://localhost/wordpress-4.5.2/?p=<?php echo $value->post_id; ?>"><?php echo get_the_title( $value->post_id ); ?></a></td>
                    <td><?php echo !empty($value->username) ? $value->username : $value->full_name; ?></td>
                    <td>
                        <?php 
                            $post = json_decode($value->details);  
                            
                            switch ($value->type) {
                                case 'fbprofile':
                                    
                                    if(isset($post->id)){
                                        if(isset($post->post_id))
                                            echo '<a href="http://facebook.com/'.@$post->post_id.'" target="_blank">'.@$post->id.'</a>';
                                        else
                                            echo '<a href="http://facebook.com/'.@$post->id.'" target="_blank">'.@$post->id.'</a>';
                                    } else {
                                        print_r($post);
                                    }

                                    break;

                                case 'fanspage':

                                    if(isset($post->id)){
                                        if(isset($post->post_id))
                                            echo '<a href="http://facebook.com/'.@$post->post_id.'" target="_blank">'.@$post->id.'</a>';
                                        else
                                            echo '<a href="http://facebook.com/'.@$post->id.'" target="_blank">'.@$post->id.'</a>';
                                    } else {
                                        print_r($post);
                                    }
                                    
                                    break;

                                case 'twitter':
                                    $usr = $post->user;
                                    echo '<a href="https://twitter.com/'.$usr->screen_name.'/status/'.$post->id.'" target="_blank">'.$post->id.'</a>';
                                    //echo '<a href="http://facebook.com/'.@$post->post_id.'" target="_blank">'.@$post->id.'</a>';
                                    break;
                                
                                default:
                                    # code...
                                    print_r(@$post->error);
                                    break;
                            }
                        ?>
                    </td> 
                </tr>
                <?php } ?> 
            </table>

            <?php
                $total = $wpdb->get_var("
                    SELECT COUNT(id)
                    FROM {$wpdb->prefix}heronative_logs 
                ");
                $page = isset( $_GET['cpage'] ) ? abs( (int) $_GET['cpage'] ) : 1;

                echo paginate_links( array(
                    'base' => add_query_arg( 'cpage', '%#%' ),
                    'format' => '',
                    'prev_text' => __('&laquo;'),
                    'next_text' => __('&raquo;'),
                    'total' => ceil($total / $comments_per_page),
                    'current' => $page
                ));

            ?> 

            </div>
        </div>

    </div>
</div>