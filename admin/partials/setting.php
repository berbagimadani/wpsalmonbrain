<div class="">
    <div class="uk-grid">
        <?php if( isset($_GET['settings-updated']) ) { ?>
        <div class="uk-width-1-1">
        <div id="message" class="updated">
                    <p><strong><?php _e('Settings saved') ?></strong></p>
        </div>
        </div>
        <?php } ?>

        <div class="uk-width-6-10 uk-margin-top"> 
            <a href="<?php echo CALL_BACK; ?>" class="uk-link-reset">
                <button class="uk-button uk-button-success label">Activation Plugins</button>
            </a> 
        </div>

        <div class="uk-width-6-10">
            <div class="uk-panel uk-panel-box box-guardian"> 
                <form method="post" action="options.php" class="uk-form uk-form-stacked"> 
                <?php
                settings_fields('wpviddycpaattributeshero'); 
                $options_key = get_option('wpviddycpaattributeshero');  
                ?>
                    <style type="text/css">
                    input[value].style-token{
                        background: #ce0c0c;
                        font-size: 18px;
                        color: #fff;
                        font-weight: bold; 
                    }
                    
                    </style>
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">* Insert Code Token For The Activation</label>
                        <div class="uk-form-controls"> 
                            <?php 
                            $gt->field( array(
                                'type'          => 'text', 
                                'name'          => 'wpviddycpaattributeshero[attributes]', 
                                'class'         => 'uk-form-large uk-width-1-1 style-token-',
                                'default'       => !empty($options_key['attributes']) ? $options_key['attributes'] : '', 
                                'placeholder'   => 'Please enter the token obtained from your purchase'
                            ));
                            ?>
                            <?php 
                            $gt->field( array(
                                'type'          => 'text', 
                                'name'          => 'wpviddycpaattributeshero[attributes2]', 
                                'class'         => 'uk-hidden',
                                'default'       => $_SERVER['SERVER_NAME'], 
                                'placeholder'   => ''
                            ));
                            ?>
                        </div>
                        <div><i>Code token has been delivery in your inbox email order</i></div>
                    </div>  

                    <hr>
 

                    <div class="uk-form-row">
                        <div class="uk-width-2-10">
                        <input type="submit" class="uk-button uk-button-success uk-button-large" value="Save"> 
                        </div>
                    </div>
               </form>
            </div>
        </div>

         <div class="uk-width-6-10">
            <div class="uk-panel uk-panel-box"> 
                    <div class="uk-form-row">
                        <label class="uk-form-label" for="form-s-it">* Generate Database</label> 
                         
                    </div>  

                    <hr>
 

                    <div class="uk-form-row">
                        <div class="uk-width-2-10">
                            <h3><?php echo @$_GET['msg']; ?></h3>
                            <a href="<?php echo CALL_BACK; ?>act=install" class="uk-button button-primary" >CLick Generate Database</a> 
                        </div>
                    </div> 


                    <div class="uk-form-row">
                        <div class="uk-width-1-1">
                            <h3><?php echo @$_GET['msg2']; ?></h3>
                            <a href="<?php echo CALL_BACK; ?>act=reset" class="uk-button uk-button-danger" >CLick Reset Database</a> 
                        </div>
                    </div> 
            </div>
        </div>

        <?php 
        if( @$_GET['act'] == 'install'){

            $msg = 'Success';

            generateTableApi(); // fb account
            generateTableSocial();
            generateTableLog();
            generateTableTwitter();

           wp_redirect(CALL_BACK.'msg='.$msg);
        }

        if( @$_GET['act'] == 'reset'){

            global $wpdb; 
            $wpdb->query("TRUNCATE TABLE {$wpdb->prefix}heronative_logs");
            //$wpdb->query("TRUNCATE TABLE {$wpdb->prefix}heronative_fb_account");
            $wpdb->query("TRUNCATE TABLE {$wpdb->prefix}heronative_social_account");
            $wpdb->query("TRUNCATE TABLE {$wpdb->prefix}heronative_twitte_account"); 

            $msg = 'reset database Success'; 
           wp_redirect(CALL_BACK.'msg2='.$msg);
        }

         
        ?>

        
    </div>
</div>

