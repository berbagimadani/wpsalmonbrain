<?php

class Wpsocialguardian_Field {
 
	public function __construct() {
 
	}

	/**
	* Render php html input
	*/
	public function render( $type, $field_array ) {

		/**
		*	TEXt
		*/
		if ( $type == 'text') {

			return '<input type="text" '.$this->parse_attr( $field_array ).' value="'.$field_array['default'].'">';	
		}

		/**
		*	TEXt BUTTON
		*/
		if ( $type == 'button') { 

			return  '<input type="button" '.$this->parse_attr( $field_array ).'>';
		}

		/**
		*	TEXt BUTTON
		*/
		if ( $type == 'button_loading') { 

			return  '<button '.$this->parse_attr( $field_array ).'> '.$field_array['value'].' <i id="loading'.$field_array['name'].'"></i></button>';
		}

		/**
		*	TEXt AREA
		*/
		if ( $type == 'textarea') {

			return '<textarea '.$this->parse_attr( $field_array ).' >'.$field_array['default'].'</textarea>';	
		}

		/**
		*	TEXt AREA TINY MCE
		*/
		if ( $type == 'wysiwyg') {

			$content = $field_array['default'];
            $settings = array( 
            	'media_buttons' => true, 
            	'textarea_name' =>  $field_array['name'],
            	'editor_height'	=> $field_array['height'],
            );
			wp_editor( $content, $field_array['name'], $settings ); 
		}

		/**
		*	TEXT COLOR FICKER
		*/
		if ( $type == 'colorpicker') {

			return '<input type="text" '.$this->parse_attr( $field_array ).' value="'.$field_array['default'].'" class="cpa-color-picker">';	
		}

		/**
		*	TEXT DATE PICKR
		*/
		if ( $type == 'text_time') {

			return '<input type="text" '.$this->parse_attr( $field_array ).' value="'.$field_array['default'].'">';	
		}

		/**
		*	FILE IMAGE
		*/
		if ( $type == 'file') {

			$preview = null;
			$html = null;
			if ($field_array['preview']) {
				$preview = '<img src="'.$field_array['default'].'" style="width:30%">';
			}

			$html .= '<input type="text" '.$this->parse_attr( $field_array ).' value="'.$field_array['default'].'">
			<input type="button" id="'.$field_array['name'].'" class="uk-button-mini button" value="Select Image" data="select">
			<input type="button" class="uk-button-mini button" id="'.$field_array['name'].'" value="Remove Image" data="remove">
			<div class="'.$field_array['name'].'" style="padding-top:10px"> </div>'.$preview;

			return $html;
		}

		/**
		*	SELECT
		*/
		if ( $type == 'select' ) {

			if ( @$field_array['page'] ) {
				return '<select '.$this->parse_attr( $field_array ).'>'.$this->parse_page_select( $field_array['page'], $field_array['default'] ).'</select>';	
			}
			if ( @$field_array['taxonomy'] ) {
				return '<select '.$this->parse_attr( $field_array ).'>'.$this->parse_tax_select( $field_array['taxonomy'], $field_array['default'] ).'</select>';	
			} 
			if ( @$field_array['options'] ){
				return '<select '.$this->parse_attr( $field_array ).'>'.$this->parse_opt_select( $field_array['options'], $field_array['default'] ).'</select>';	
			}
		}

		/**
		*	SELECT POPULATE
		*/
		if ( $type == 'select_populate' ) {

			if ( $field_array['page'] ) {
				
				$n = null;
				foreach ($field_array['chained'] as $key => $value) { $n[]= $value; }

				return '<select count="'.implode(',', $n).'" '.$this->parse_attr( $field_array ).'>'.$this->parse_page_select_populate( $field_array['page'], $field_array['default'], $field_array['opt_select'], $field_array['chained'] ).'</select>';	
			
			} 

			if ( $field_array['taxonomy'] ) {
				
				$n = null;
				foreach ($field_array['chained'] as $key => $value) { $n[]= $value; }

				return '<select count="'.implode(',', $n).'" '.$this->parse_attr( $field_array ).'>'.$this->parse_tax_select_populate( $field_array['taxonomy'], $field_array['default'], $field_array['opt_select'], $field_array['chained'] ).'</select>';	
			
			} 
			if ( $field_array['options'] ){

				$n = null;
				foreach ($field_array['chained'] as $key => $value) { $n[]= $value; }	

				return '<select count="'.implode(',', $n).'" '.$this->parse_attr( $field_array ).'>'.$this->parse_opt_select_populate( $field_array['options'], $field_array['default'], $field_array['opt_select'], $field_array['chained'] ).'</select>';	
			}
		}

		if ( $type == 'label' ) {

			$html = null;
			foreach ( $field_array['title'] as $key => $value ) {
				
				$html .= ' <label name="'.$field_array['name'].'" id="'.$key.'" class="uk-hidden">'.$value.'</label>';
			}
			
			return $html;
		}

		/**
		*	RADIO
		*/
		if ( $type == 'radio' ) {

			if ( @$field_array['taxonomy'] ) {
				
				$categories = $this->get_taxonomy( $field_array['taxonomy'] );
				$html = null;
				foreach ( $categories as $row ) { 
					$html .= '<label><input type="radio"  '.$this->parse_attr( $field_array ).' value='.$row->term_id.' '.checked( $row->term_id, $field_array['default'], false ).'> <span> '.$row->cat_name.' </span></label>';
				}

				return $html;

			} 
			if ( @$field_array['options'] ) {

				$html = null;
				foreach ($field_array['options'] as $key => $value) {
					$html .= '<label><input type="radio"  '.$this->parse_attr( $field_array ).' value='.$key.' '.checked( $key, $field_array['default'], false ).'> <span> '.$value.' </span></label>';
				}

				return $html;

			}
		}

		/**
		*	RADIO
		*/
		if ( $type == 'checkbox' ) {

			$html = null;
			if ( @$field_array['taxonomy'] ) {
				
				$categories = $this->get_taxonomy( $field_array['taxonomy'] );

				foreach ( $categories as $row ) { 
					$html .= '<label><input type="checkbox"  '.$this->parse_attr( $field_array ).' value='.$row->term_id.' '.$this->multiple_default( $field_array['default'], $row->term_id ).'> <span> '.$row->cat_name.' </span></label>';
				}

				return $html;

			} 
			if ( $field_array['options'] ) {
 
				foreach ($field_array['options'] as $key => $value) {

					$html .= '<label><input type="checkbox"  '.$this->parse_attr( $field_array ).' value='.$key.' '.$this->multiple_default( $field_array['default'], $key ).'> <span> '.$value.' </span></label>';
				}

				return $html;

			}
		}

	}

	/**
	* parse attributes html 
	*/
	public function parse_attr( $attr ) {

		$attributes = null;
		foreach ( $attr as $key => $value ) {

			if ( $key != 'options' && $key != 'default' && $key != 'chained' && $key != 'opt_select' )  {
				
				if ( $key == 'name' ) {
					$attributes  .= 'id='.'"'.$value.'"';
					$attributes  .= $key.'='.'"'.$value.'"';
				
				} else{
					$attributes  .= $key.'='.'"'.$value.'"';
				}
			}
		}
		return $attributes;
	}

	/**
	* parse option select
	*/
	public function parse_opt_select( $options, $default = null ) {

		$attributes = null;
		foreach ( $options as $key => $value ) {

			$attributes  .= '<option value="'.$key.'" '.selected( $key, $default, false ).'>'.$value.'</option>';
		}
		return $attributes;
	}

	/**
	* parse option select populate
	*/
	public function parse_opt_select_populate( $options, $default = null, $select = null, $chained = null ) {
 	
 		$attributes = null;
		if ( !empty( $select )) 
		$attributes = '<option value="0" id="0" data="0">'.$select.'</option>';

		foreach ( $options as $key => $value ) {
			$attributes  .= '<option value="'.$key.'" id="'.$key.'" data="'.$chained[$key].'">'.$value.'</option>';
		}
		return $attributes;
	}

	/**
	* parse page option select
	*/
	public function parse_page_select( $page, $default = null ) {
 		
		$page = $this->get_page( $page );

		foreach ( $page as $row ) {
			$opts .= '<option value='.$row->ID.' '.selected( $row->ID, $default, false ).'>'.$row->post_title.'</option>';
		}
		 
		return $opts;
	}

	/**
	* parse taxonomy option select
	*/
	public function parse_tax_select( $tax, $default = null ) {
 		
		$categories = $this->get_taxonomy( $tax );
		$opts = null;
		foreach ( $categories as $row ) {
			$opts .= '<option value='.$row->term_id.' '.selected( $row->term_id, $default, false ).'>'.$row->cat_name.' ('.$row->count.')</option>';
		}
		 
		return $opts;
	}

	/**
	* parse taxonomy option select
	*/
	public function parse_tax_select_populate( $tax, $default = null, $select = null, $chained = null ) {
 		
		$categories = $this->get_taxonomy( $tax );

		$opts = null;
		if ( !empty( $select )) 
		$opts .= '<option value="0" id="0" data="0">'.$select.'</option>';


		$opts .= '<option value="create" id="create" data="#create_page">[ Create New Page ]</option>';
		foreach ( $categories as $row ) {
			$opts .= '<option value='.$row->term_id.' id="'.$row->term_id.'" data="'.$chained[$row->term_id].'">'.$row->cat_name.' ('.$row->count.')</option>';
		}
		 
		return $opts;
	}
    
    /**
	* parse page option select
	*/
	public function parse_page_select_populate( $page, $default = null, $select = null, $chained = null ) {
 		
		$pages = $this->get_page( $page );

		$opts = null;
		if ( !empty( $select )) 
		$opts .= '<option value="0" id="0" data="0">'.$select.'</option>';

		$opts .= '<option value="create" id="create" data="'.$chained['{create_new}'].'">[ Create New Page ]</option>';
		foreach ( $pages as $row ) {
			$opts .= '<option value='.$row->ID.' id="'.$row->ID.'" data="'.$chained[$row->ID].'">'.$row->post_title.'</option>';
		}
		 
		return $opts;
	}

	/**
	*	Get multiple default value 
	*
	*/
   	public function multiple_default( $default, $key ){

   		$sel = null;
		if ( is_array( $default ) ) {
			foreach ($default as  $row) {
				if ( $row == $key ) {
					$sel = checked( $row, $key, false );
				}
			}

		return $sel;
		}
   	}

   	/**
	*	Get PAge
	*
	*/
	public function get_page( $page ) {

		$args = array( 
			'sort_order'        => 'DESC', 
			'sort_column' 		=> 'post_title',
			'post_type' 		=> $page,
			'post_status' 		=> 'publish'
		); 
 
		$pages = get_pages( $args );
		if ( $pages ) {
			
			return $pages;
		}
	}

	/**
	*	Get Taxonomy 
	*
	*/
	public function get_taxonomy( $tax ) {

		$args = array( 
			'orderby'            => 'name',
			'order'              => 'DESC', 
			'show_count'         => 1,
			'hide_empty'         => 0, 
			'taxonomy'           => $tax, 
		); 

		$opts = null; 
		$categories = get_categories( $args );
		if ( $categories ) {
			
			return $categories;
		}
	}

	/**
	*	Get Html markup
	*
	*/
	public function get_html_markup( $input ) {

	}

	public function field( array $field) { 

		foreach ( $field as $key => $value ) {
			echo $this->render( $value, $field );
		}
 
	}


}


?>