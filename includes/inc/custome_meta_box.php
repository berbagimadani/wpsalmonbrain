<?php

//making the meta box (Note: meta box != custom meta field)
function wpse_add_custom_meta_box_hero() {

   add_meta_box(
       'custom_meta_box_wp_hero_native',       // $id
       'Wp Salmon Brain :: Find content Viral and Curate',         // $title
       'show_custom_meta_wp_hero_native',  // $callback
       'post',                  // $page
       'normal',                  // $context
       'high'                     // $priority
   );
}
add_action('add_meta_boxes', 'wpse_add_custom_meta_box_hero');

function dataFieldWpsocialguardian() {
        
	/* optional */
	$data['keyword' ]    = ''; 
  $data['table' ]    = '';
  $data['auto_share' ]    = '';

	return $data;
}

function show_custom_meta_wp_hero_native() {
    global $post;

    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'inc/class-wpsocialguardian-field.php'; 
	$gt = new wpsocialguardian_Field(); 

	$plugin_meta = 'wpsocialguardian-meta';
    // Use nonce for verification to secure data sending
    wp_nonce_field( basename( __FILE__ ), 'wpse_our_nonce' );
   
    $options = get_option('wpsocialguardian-option'); 

    ?>
 	
 	<div class="uk-form-stacked-custome uk-form-horizontal">
 	<?php foreach ( dataFieldWpsocialguardian() as $key => $value) { ?>
  
     <?php if ($key == 'keyword') { ?>
     <div class="uk-grid uk-grid-collapse uk-margin-top uk-panel uk-panel-box-secondary">
        <div class="uk-width-1-1"> 
          <select class="uk-form-large" id="salmon-limit"> 
            <?php
            for ($i=1; $i <=100 ; $i++) { 
              if($i % 10 == 0){
                echo '<option value="'.$i.'">'.$i.'</option>';
              }
            }
            ?> 
          </select>
          <select class="uk-form-large" id="salmon-source">
            <option value="google.co.id">Google.co.id</option>
            <option value="google.com">Google.com</option>
            <option value="google.fr">Google.fr</option>
          </select>
          <?php 
                $gt->field( array(
                    'type'          => 'text', 
                    'name'          => 'salmon-keyword', 
                    'class'         => 'uk-width-6-10 uk-form-large',
                    'default'       => 'pengobatan asam urat',
                    'placeholder'   => 'Keyword'
                ));
          ?>
           <button type="button" class="uk-button uk-button-large uk-button-success" id="salmon-search">Search</button>
          <span id="loading"></span>
        </div> 
      </div>
      <div class="uk-grid uk-grid-collapse">
        <div class="uk-width-1-1">   
          <span id="salmon-related"></span> 
        </div>
      </div>
      <?php } ?>

    <?php if ($key == 'table') { ?> 
     <div class="uk-form-row uk-margin-top uk-margin-bottom">      
        <table class="uk-table uk-table uk-table-hover uk-table-striped" data-sortable> 
            <thead style="background:#F1F1F1">
                <tr>
                    <th>URL Title</th>
                    <th>Facebook</th>
                    <th>Google</th>
                    <th>Pinterest</th> 
                    <th>Linkedin</th>
                    <th>Total</th>
                </tr>
            </thead> 
            <tbody class="result">
                <tr></tr>
            </tbody>
        </table>


        <!--<div class="uk-modal">
          <div class="uk-modal-dialog">
            <a class="uk-modal-close uk-close"></a>
            <h2 class="title"></h2>

            <div class="uk-grid">
              <div class="uk-width-3-10">
                <label>CSS Selector*</label>
                <input type="text" class="datas">
                <input type="text" class="uk-width-1-1" id="salmon-css-selector">
              </div>
               <div class="uk-width-7-10">
                <span class="loading"></span>
                <span id="preview">..</span>
              </div>
            </div>
            <div class="uk-grid">
              <div class="uk-width-1-1">
              <button id="salmon-curate-post" type="button" class="uk-button uk-button-success">Curate</button>
              <button id="salmon-insert-post" type="button">POST</button>
              </div>
             </div>

          </div>
        </div>-->

 
     </div> 
     <?php } ?> 



     <?php 
      if ($key == 'auto_share') {
            global $wpdb;  
            $user_count = $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}heronative_fb_account where status is Null" );
            $fans = $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}heronative_social_account where status is Null" );
            $twit = $wpdb->get_var( "SELECT COUNT(*) FROM {$wpdb->prefix}heronative_twitter_account where status is Null" );
        
     ?> 
      <hr>
       <div class="uk-form-row">
         <label class="uk-form-label" for="form-s-it"><?php echo sanitizeStringForUrlHero($key); ?></label>
          <div class="uk-form-controls"> 
              <p><div class="uk-badge uk-badge-notification"><?php echo $user_count ?></div> facebook profile ready to share </p>
              <p><div class="uk-badge uk-badge-notification"><?php echo $fans ?></div> fans page ready to share </p> 
          </div>
       </div>
      

    <?php } ?>
 	<?php } ?>
 	</div>

    <?php
}

function sanitizeStringForUrlHero($string){
    $string = str_replace(array('_'),array(' '),$string);
    return ucwords(strtolower($string));
}

//now we are saving the data
function wpse_save_meta_fields_wp_hero_native( $post_id ) {
 
$plugin_meta = 'wpsocialguardian-meta';

  // verify nonce
  if (!isset($_POST['wpse_our_nonce']) || !wp_verify_nonce($_POST['wpse_our_nonce'], basename(__FILE__)))
      return 'nonce not verified';

  // check autosave
  if ( wp_is_post_autosave( $post_id ) )
      return 'autosave';

  //check post revision
  if ( wp_is_post_revision( $post_id ) )
      return 'revision';

  if ( 'page' == $_POST['post_type'] ) {
    if ( !current_user_can( 'edit_page', $post_id ) )
    return;
  } else {
    if ( !current_user_can( 'edit_post', $post_id ) )
    return;
  }

  	foreach ( dataFieldWpsocialguardian() as $key => $value) { 

  		//if ($key != 'player' & $key != 'type_cta' & $key != 'locker' & $key != 'image' & $key != 'url_image' & $key != 'play' & $key != 'download') {
  			$field_name = $_POST['wpsocialguardian-meta-'.$key];
  			update_post_meta( $post_id, 'wpsocialguardian-meta-'.$key, $field_name );
  		//}
	}

}
add_action( 'save_post', 'wpse_save_meta_fields_wp_hero_native' );
add_action( 'new_to_publish', 'wpse_save_meta_fields_wp_hero_native' );

?>