<?php

/**
 * Fired during plugin activation
 *
 * @link       kabarharian.com
 * @since      1.0.0
 *
 * @package    Wpsalmonbrain
 * @subpackage Wpsalmonbrain/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wpsalmonbrain
 * @subpackage Wpsalmonbrain/includes
 * @author     Ade iskandar <berbagimadani@gmail.com>
 */
class Wpsalmonbrain_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
