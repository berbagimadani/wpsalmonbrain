<?php

/**
 * Fired during plugin deactivation
 *
 * @link       kabarharian.com
 * @since      1.0.0
 *
 * @package    Wpsalmonbrain
 * @subpackage Wpsalmonbrain/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wpsalmonbrain
 * @subpackage Wpsalmonbrain/includes
 * @author     Ade iskandar <berbagimadani@gmail.com>
 */
class Wpsalmonbrain_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
