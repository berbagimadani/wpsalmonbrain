<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       kabarharian.com
 * @since      1.0.0
 *
 * @package    Wpsalmonbrain
 * @subpackage Wpsalmonbrain/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Wpsalmonbrain
 * @subpackage Wpsalmonbrain/includes
 * @author     Ade iskandar <berbagimadani@gmail.com>
 */
class Wpsalmonbrain_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'wpsalmonbrain',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
