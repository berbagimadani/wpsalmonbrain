<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              kabarharian.com
 * @since             1.0.0
 * @package           Wpsalmonbrain
 *
 * @wordpress-plugin
 * Plugin Name:       Salmon Brain
 * Plugin URI:        salmonbrain.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Ade iskandar
 * Author URI:        kabarharian.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wpsalmonbrain
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wpsalmonbrain-activator.php
 */
function activate_wpsalmonbrain() {
	
	$plugin = new Wpsalmonbrain();

	$deprecated = null;
  	$autoload = 'yes';

	if ( get_option( $plugin->get_plugin_slug() ) !== false ) {
	    update_option( $plugin->get_plugin_slug(), '' ); 
	    
	    //generateTableApi(); // fb account
	    //generateTableSocial();
	    //generateTableLog();
	    //generateTableTwitter();

	}
	else{
		add_option( $plugin->get_plugin_slug(), '', $deprecated, $autoload );  
	}
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wpsalmonbrain-deactivator.php
 */
function deactivate_wpsalmonbrain() {
	
	$plugin = new Wpsalmonbrain();
	update_option( $plugin->get_plugin_slug(), 'closed' ); 

}

function generateTableApi() {
	global $wpdb;
	$table_name = $wpdb->prefix.'heronative_fb_account';
	$charset_collate = $wpdb->get_charset_collate();
		
		$sql = "CREATE TABLE $table_name (
		    id mediumint(9) NOT NULL AUTO_INCREMENT, 
		    name_apps text NOT NULL,
		    app_id text NOT NULL,
		    app_secret text NOT NULL,  
		    full_name text NOT NULL,
		    uid text NOT NULL, 
		    access_token text NOT NULL, 
		    status mediumint(9),
		    UNIQUE KEY id (id)
		) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
}

function generateTableTwitter() {
	global $wpdb;
	$table_name = $wpdb->prefix.'heronative_twitter_account';
	$charset_collate = $wpdb->get_charset_collate();
		
		$sql = "CREATE TABLE $table_name (
		    id mediumint(9) NOT NULL AUTO_INCREMENT, 
		    username text NOT NULL,
		    full_name text NOT NULL,
		    uid text NOT NULL, 
		    access_token text NOT NULL,
		    secret_token text NOT NULL,   
		    status mediumint(9),
		    UNIQUE KEY id (id)
		) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
}	

function generateTableSocial() {
	global $wpdb;
	$table_name = $wpdb->prefix.'heronative_social_account';
	$charset_collate = $wpdb->get_charset_collate();
		
		$sql = "CREATE TABLE $table_name (
		    id mediumint(9) NOT NULL AUTO_INCREMENT,   
		    fb_account_id text NOT NULL,
		    type text NOT NULL, 
		    page_id text NULL, 
		    full_name text NULL, 
		    username VARCHAR(220) NULL, 
		    access_token text NULL, 
		    status mediumint(9),
		    UNIQUE KEY id (id)
		) $charset_collate;";
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );

}	

function generateTableLog() {
	global $wpdb;
	$table_name = $wpdb->prefix.'heronative_logs';
	$charset_collate = $wpdb->get_charset_collate();
		
		$sql = "CREATE TABLE $table_name (
		    id mediumint(9) NOT NULL AUTO_INCREMENT, 
		    post_id mediumint(9),
		    app_id text NOT NULL, 
		    type VARCHAR(220) NULL,
		    full_name text NOT NULL,
		    username VARCHAR(220) NULL,
		    uid text NOT NULL, 
		    details text NOT NULL, 
		    UNIQUE KEY id (id)
		) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
}	

 
register_activation_hook( __FILE__, 'activate_wpsalmonbrain' );
register_deactivation_hook( __FILE__, 'deactivate_wpsalmonbrain' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wpsalmonbrain.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wpsalmonbrain() {

	$plugin = new Wpsalmonbrain();
	$plugin->run();

}
run_wpsalmonbrain();
